import React from 'react';

const History = (props) => {
  const historyList = props.list.map(x => <li key={x}> {x} </li>);

  return(
    <section>
      <h2> Geschiedenis </h2>
      <ul>{historyList}</ul>
    </section>
  );
}

export default History;
