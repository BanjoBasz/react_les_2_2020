import React from 'react';
import "./ImgCard.css"
const ImgCard = (props) => {
  return (
    <section className="imgCardSection">
      <figure className="imgCardSection__figure">
        <img className="imgCardSection__figure__img" src={props.imgSrc} alt="Image of a Pokémon" />
        <figcaption className="imgCardSection__figure__figcaption">
          {props.name || "Geen naam beschikbaar"} <br />
          {props.number || "Geen nummer beschikbaar"}
        </figcaption>
      </figure>
    </section>
  );
}

export default ImgCard;
