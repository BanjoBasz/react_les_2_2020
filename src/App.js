import React from "react";
import SearchBar from "./SearchBar";
import ImgCard from "./ImgCard";
import History from "./History";
import "./App.css";

import axios from "axios";

class App extends React.Component {
  state = { name: "", img: "", number: "", list: []};

  onSubmit = (searchTerm) => {
    const BASE_URL = "https://pokeapi.co/api/v2/pokemon/"
    axios.get(BASE_URL + searchTerm).then(res => {
      this.setState({
        name: res.data.name.charAt(0).toUpperCase() + res.data.name.slice(1),
        number: res.data.id,
        img: res.data.sprites.front_default,
        list: [ ...this.state.list,  res.data.name.charAt(0).toUpperCase() + res.data.name.slice(1)]
      });
    });
  }

  render(){
    let imgCard;
    let historyList;

    if(this.state.list.length > 0){
      historyList =  <History list={this.state.list}/>;
    }

    if(this.state.img !== ""){
      imgCard =  <ImgCard name={this.state.name} number={this.state.number} imgSrc={this.state.img} />;
    }

    return (
      <article>
        <header>
          <figure>
            <img src="/img/logo.jpg" alt="Logo van Pokémon"/>
          </figure>
        </header>
        <SearchBar onSubmit={this.onSubmit}  />
        {imgCard}
        {historyList}
      </article>
    );
  }

}

export default App;
