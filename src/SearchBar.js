import React from "react";
import "./SearchBar.css";

class SearchBar extends React.Component {
  state = {searchTerm: ""};

  onSearch = (event) => {
    this.setState({searchTerm: event.target.value});
  }

  onSubmit = (event) =>{
    event.preventDefault();
    this.props.onSubmit(this.state.searchTerm);
  }
  render(){
    return (
      <section class="searchbar_section">
        <form onSubmit={this.onSubmit} >
          <input onChange={this.onSearch} class="searchbar_section__input" placeholder="Zoek hier op naam of nummer" type="text" value={this.state.searchTerm}/>
        </form>
      </section>
    );
  }
}

export default SearchBar;
